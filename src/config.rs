use ini::Ini;

#[derive(Debug, Clone)]
pub struct Configuration {
    pub database_path: String,
    pub log_file_path: String,
    pub deletion_time_threshold: u32,
    pub max_command_length: u16,
    pub use_pinnings: bool,
    pub log_level: LogLevel,
}

impl Configuration {
    pub fn get_default() -> Result<Configuration, String> {
        let mut config = Configuration {
            log_level: LogLevel::WARNING,
            use_pinnings: true,
            database_path: "~/.dirhist.db".to_string(),
            log_file_path: "~/.dirhist.log".to_string(),
            max_command_length: 4096,
            deletion_time_threshold: 2592000,
        };

        match home::home_dir() {
            Some(path) => {
                let home_dir = path.into_os_string().into_string().unwrap();
                config.database_path = String::clone(&home_dir) + "/.dirhist.db";
                config.log_file_path = home_dir + "/.dirhist.log";

                Ok(config)
            }
            None => Err("Cannot read home directory path".to_string()),
        }
    }
}

#[derive(Debug, Clone)]
pub enum LogLevel {
    TRACE,
    DEBUG,
    INFO,
    WARNING,
    ERROR,
}

impl LogLevel {
    pub fn from_str(v: &str) -> Result<LogLevel, String> {
        match v {
            "trace" => Ok(LogLevel::TRACE),
            "debug" => Ok(LogLevel::DEBUG),
            "info" => Ok(LogLevel::INFO),
            "warning" => Ok(LogLevel::WARNING),
            "error" => Ok(LogLevel::ERROR),
            _ => Err("Invalid log level {}".to_string()),
        }
    }
}

impl Configuration {
    pub fn load(path: String) -> Result<Configuration, String> {
        match Configuration::get_default() {
            Ok(config) => match process_configuration_file(path, &config) {
                Ok(modified_config) => Ok(modified_config),
                Err(e) => {
                    println!("{} Falling back to default configuration.", e);
                    Ok(config)
                }
            }
            Err(e) => Err(e),
        }
    }
}

fn process_configuration_file(
    path: String,
    config: &Configuration,
) -> Result<Configuration, ini::Error> {
    match Ini::load_from_file(path) {
        Err(e) => {
            println!("Cannot open configuration file {}", e);
            Err(e)
        }
        Ok(content) => {
            let mut modified_config = Configuration::clone(config);

            for (_sec, prop) in content.iter() {
                for (k, v) in prop.iter() {
                    process_configuration_entry(&mut modified_config, k, v);
                }
            }

            Ok(modified_config)
        }
    }
}

fn process_configuration_entry(config: &mut Configuration, k: &str, v: &str) {
    match k {
        "log_level" => {
            config.log_level = match LogLevel::from_str(v) {
                Ok(v) => v,
                Err(e) => {
                    println!("{} Falling back to default value.", e);
                    LogLevel::WARNING
                }
            }
        }
        "use_pinnings" => config.use_pinnings = v == "true",
        "database_path" => config.database_path = v.to_string(),
        "log_file_path" => config.log_file_path = v.to_string(),
        "max_command_length" => {
            config.max_command_length = match u16::from_str_radix(v, 10) {
                Err(_e) => {
                    println!("Invalid max_command_length {}. Falling back to default.", v);
                    config.max_command_length
                }
                Ok(v) => v,
            }
        }
        "deletion_time_threshold" => {
            config.deletion_time_threshold = match u32::from_str_radix(v, 10) {
                Err(_e) => {
                    println!(
                        "Invalid deletion_time_threshold {}. Falling back to default.",
                        v
                    );
                    config.deletion_time_threshold
                }
                Ok(v) => v,
            }
        }
        _ => println!("Invalid configuration {}. Ignoring it...", k),
    }
}
