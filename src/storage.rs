pub trait InitializeStorage {
    fn initialize(connection_string: String);
}

pub trait HistoryStorage: InitializeStorage {

}

pub trait PinningStorage: InitializeStorage {
}
