use std::env;

mod cli;
use std::process::exit;

use cli::{Args, Commands};

mod config;
use config::Configuration;

mod history;
mod pinnings;

enum ErrorCodes {
    OK = 0,
    InvalidConfiguration,
    FailedToGetPath,
}

impl Into<i32> for ErrorCodes {
    fn into(self) -> i32 {
        match self {
            ErrorCodes::OK => 0,
            ErrorCodes::FailedToGetPath => 1,
            ErrorCodes::InvalidConfiguration => 2,
        }
    }
}

fn main() {
    let args = Args::parse_arguments();
    let config = match Configuration::load(args.configuration_file_path) {
        Err(e) => {
            println!("Cannot read configuration file. {}", e);
            exit(ErrorCodes::InvalidConfiguration.into());
        }
        Ok(c) => c,
    };

    match args.command {
        None => match env::current_dir() {
            Ok(current_dir) => history::list(&current_dir, None),
            Err(error) => {
                println!("Error while reading the current directory {}", error);
                exit(ErrorCodes::FailedToGetPath.into());
            }
        },
        Some(c) => match c {
            Commands::Add { command } => match env::current_dir() {
                Ok(current_dir) => history::add(&current_dir, &command),
                Err(error) => {
                    println!("Error while reading the current directory {}", error);
                    exit(ErrorCodes::FailedToGetPath.into());
                }
            },
            Commands::Show { limit } => match env::current_dir() {
                Ok(current_dir) => history::list(&current_dir, limit),
                Err(error) => {
                    println!("Error while reading the current directory {}", error);
                    exit(ErrorCodes::FailedToGetPath.into());
                }
            },
            Commands::CleanUp {} => history::clean_up(),
            Commands::PathSearch {
                regular_expression,
                limit,
            } => history::path_search(regular_expression, limit),
            Commands::CommandSearch {
                regular_expression,
                limit,
            } => history::command_search(regular_expression, limit),
            Commands::Pin { command } => match env::current_dir() {
                Ok(current_dir) => pinnings::pin(&current_dir, &command),
                Err(error) => {
                    println!("Error while reading the current directory {}", error);
                    exit(ErrorCodes::FailedToGetPath.into());
                }
            },
            Commands::UnPin { pinning_id } => pinnings::unpin(pinning_id),
        },
    }

    exit(ErrorCodes::OK.into());
}
