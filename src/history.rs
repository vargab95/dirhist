use chrono::{DateTime, Utc};
use regex::Regex;
use std::collections::LinkedList;
use std::path::Path;

pub struct HistoryRecord {
    path: String,
    command: String,
    timestamp: DateTime<Utc>,
}

pub struct History {
    records: LinkedList<HistoryRecord>,
}

pub fn add(path: &Path, command: &String) {
    println!("New path {} {}", path.to_str().unwrap(), command);
}

pub fn list(path: &Path, limit: Option<u32>) {
    println!("List {}", path.to_str().unwrap());
}

pub fn clean_up() {
    println!("Cleaning");
}

pub fn path_search(regular_expression: Option<Regex>, limit: Option<u32>) {
    println!("Path search {}", regular_expression.unwrap());
}

pub fn command_search(regular_expression: Option<Regex>, limit: Option<u32>) {
    println!("Command search {}", regular_expression.unwrap());
}
