use home;

use regex::Regex;

use clap::Parser;
use clap_derive::{Parser, Subcommand};

/// Saves and searches history entries in a per directory basis.
#[derive(Parser)]
#[command(author, version, about, long_about = None)]
pub struct Args {
    /// Sets a custom config file
    #[arg(short, long, value_name = "FILE", default_value = "~/.dirhist.ini")]
    pub configuration_file_path: String,

    #[command(subcommand)]
    pub command: Option<Commands>,
}

#[derive(Subcommand)]
pub enum Commands {
    /// Adds a new entry to the directory history.
    Add {
        /// The command to store in the history database
        #[arg(last = true)]
        command: String,
    },

    /// Searches the history entries by path. Regular expressions can be used.
    PathSearch {
        /// The regular expression which should be used to search the pathes
        #[arg(last = true)]
        regular_expression: Option<Regex>,

        /// Maximum number of printed history entries
        #[arg(short, long)]
        limit: Option<u32>,
    },

    /// Searches the history entries by command. Regular expressions can be used.
    CommandSearch {
        /// The regular expression which should be used to search the commands
        #[arg(last = true)]
        regular_expression: Option<Regex>,

        /// Maximum number of printed history entries
        #[arg(short, long)]
        limit: Option<u32>,
    },

    /// Creates a pinning to the current folder.
    Pin {
        /// The command to pin to the current folder
        #[arg(last = true)]
        command: String,
    },

    /// Removes a pinning of the current folder.
    UnPin {
        /// The id of the pinning which should be deleted
        #[arg(last = true)]
        pinning_id: u32,
    },

    /// Cleans up the old history entries from the database based on the configuration.
    CleanUp {},

    /// Shows the history of the current folder.
    Show {
        /// Maximum number of printed history entries
        #[arg(short, long)]
        limit: Option<u32>,
    },
}

impl Args {
    pub fn parse_arguments() -> Self {
        let mut args = Args::parse();

        match home::home_dir() {
            Some(path) => {
                args.configuration_file_path =
                    path.into_os_string().into_string().unwrap() + "/.dirhist.ini"
            }
            None => println!("Cannot get your home dir! Please specify configuration file path."),
        }

        args
    }
}
