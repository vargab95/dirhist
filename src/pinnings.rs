use chrono::{DateTime, Utc};
use regex::Regex;
use std::collections::LinkedList;
use std::path::Path;

pub struct Pinning {
    id: u32,
    path: String,
    command: String,
}

pub fn pin(path: &Path, command: &String) {
    println!("New pinning {} {}", path.to_str().unwrap(), command);
}

pub fn unpin(id: u32) {
    println!("Unpin {}", id);
}

pub fn list(path: &Path, limit: Option<u32>) {
    println!("List {}", path.to_str().unwrap());
}
